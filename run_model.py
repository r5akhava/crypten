from ast import Global
from pydoc import plain
from random import seed
import crypten
import torch
import torch.nn as nn
import random

from models.mnist.mnist import MNIST
from models.mnist.mlp import MLP
from models.resnet.cifar_resnet import ResNet18
from models.resnet.cifar import CIFAR10

import GLOBALS
import numpy as np

import os
import pickle
# Ignore warnings
import warnings

import torch

import GLOBALS
import crypten
from models.mnist.mlp import MLP
from models.mnist.mnist import MNIST
from models.resnet.cifar import CIFAR10
from models.resnet.cifar_resnet import ResNet18

warnings.filterwarnings("ignore")

import argparse

def set_seed(my_seed):
    random.seed(my_seed)
    np.random.seed(my_seed)
    torch.manual_seed(my_seed)

def parse_args():
    parser = argparse.ArgumentParser(description="train a model on a dataset.")
    # --! Training parameters !--
    parser.add_argument("-n", "--model_name", default="resnet", type=str, help="name of the model",
                        choices=["mnist", "resnet"])
    parser.add_argument("-p", "--poly_file", type=str, help="file which contains the coefficients of the polynomial")
    parser.add_argument("--dataset", default="cifar10", type=str, choices=["cifar10"])
    # --! Env parameters !--
    parser.add_argument("-e", "--epoch", type=int, default=100,
                        help="num of epochs to train")
    parser.add_argument("--batch_size", type=int, default=1,
                        help="batch size for the GAN.")
    parser.add_argument("-a", "--act_type", default="relu", type=str, help="activation type")
    parser.add_argument("-d", "--degree", default=2, type=int, help="degree of polynomial")
    parser.add_argument("-r", "--range", default=2.0, type=float, help="range of polynomial")
    parser.add_argument("--reg_range", default=1.5, type=float, help="range used for regularization")
    parser.add_argument("-c", "--reg_coef", default=0.0, type=float, help="the coefficient for regularization")
    parser.add_argument("-q", "--to_quant", default=0, type=int, help="boolean whether to use quantization")
    parser.add_argument("--clip", default=-1, type=float, help="the range used for clipping")
    parser.add_argument("--epoch_to_swap", default=-1, type=int, help="the epoch number to swap to poly act funcs")
    parser.add_argument("--fine_tune", default=0, type=int, help="whether we are fine tuning or hot swapping")
    parser.add_argument("--penalty_exp", default=10.0, type=float, help="the exponent for the penalty function")
    parser.add_argument("--auto_augment", default=0, type=int, help="whether to turn on auto_augment")
    parser.add_argument("--image_resize", default=0, type=int, help="whether to resize the input to 224*224")
    parser.add_argument("--lr", default=0.001, type=float, help="initial learning rate")
    parser.add_argument("--opt", default='sgd', type=str, help="optimizer type")
    parser.add_argument("--weight_decay", default=0.0, type=float, help="the weight decay for the optimizer")
    # --! Output parameters !--
    parser.add_argument("--output_folder", type=str, default="./outputs/",
                        help="output folder for the model checkpoint.")
    return parser.parse_args()

if __name__=='__main__':

    # set_seed(3)

    # Define ALICE and BOB src values
    ALICE = 0
    BOB = 1

    crypten.init()
    torch.set_num_threads(1)
    device = torch.device("cpu")


    args = parse_args()

    if os.path.exists(args.poly_file):
        with open(args.poly_file, 'rb') as input_file:
            GLOBALS.COEFFS = pickle.load(input_file)

    if args.model_name == 'resnet':
        sample_trained_model_file = "models/resnet/best_poly_resnet.pth"
        model_class=ResNet18(args.act_type, degree=args.degree, rnge=args.range, clip=args.clip).to(device)
        plain_model=ResNet18('relu_poly', degree=args.degree, rnge=args.range, clip=args.clip).to(device)
        model_class = ResNet18(args.act_type, degree=args.degree, rnge=args.range, clip=args.clip).to(device)
        plain_model = ResNet18('relu_poly', degree=args.degree, rnge=args.range, clip=args.clip).to(device)
        dataset = CIFAR10(root='data/')
    elif args.model_name=='mnist':
        sample_trained_model_file = 'models/mnist/arelu_poly_d2_r2.0_mlp_state_dict.pth'
        model_class = MLP(args).to(device)
        plain_model=MLP(args).to(device)
        dataset = MNIST(root='data/')        

    while GLOBALS.COEFFS[-1] == 0.0:
        GLOBALS.COEFFS=GLOBALS.COEFFS[:-1]
    # GLOBALS.COEFFS=GLOBALS.COEFFS
    print(GLOBALS.COEFFS)

    model = crypten.load_from_party(sample_trained_model_file, model_class=model_class, src=ALICE)
    plain_model.load_state_dict(torch.load(sample_trained_model_file, map_location=torch.device('cpu')))
    test_loader = dataset.get_dataloader('valid', batch_size=args.batch_size, num_workers=1, pin_memory=False)

    dummy_input, dummy_labels = next(iter(test_loader))
    private_model = crypten.nn.from_pytorch(model, dummy_input)

    model.eval()
    plain_model.eval()

    import crypten.mpc as mpc

    @mpc.run_multiprocess(world_size=2)
    def examine_arithmetic_shares():

        # Encrypt the network
        private_model.encrypt(src=ALICE)
        
        # Check that model is encrypted:
        print("Model successfully encrypted:", private_model.encrypted)
        
        # Encrypt the input
        encrypted_input = crypten.cryptensor(dummy_input, src=BOB, ptype=crypten.mpc.arithmetic)

        private_model.eval()
        output=private_model(encrypted_input)
        
        return output.get_plain_text()

    true_outputs=plain_model(dummy_input)
    crypten_outputs = examine_arithmetic_shares()[0]

    print("--------Example--------")
    print('True:', true_outputs[0])
    print('Crypten:', crypten_outputs[0])
    print('Error:', abs(true_outputs-crypten_outputs)[0])
    print("-----------------------")

    true_pred = true_outputs.argmax(dim=1)
    print("True Preds:   ", true_pred)
    pred = crypten_outputs.argmax(dim=1)
    print("Crypten Preds:", pred)
    print("True labels:", dummy_labels)

    print("Match CrypTen with Plain: {:3f} %".format(sum(pred==true_pred)/len(pred)*100))
    print("Plain Acc: {:3f}".format(sum(dummy_labels==true_pred)/len(pred)*100))
    print("Crypten Acc: {:3f}".format(sum(dummy_labels==pred)/len(pred)*100))
