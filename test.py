# import the libraries
import pickle
import random
from math import ceil
from tqdm import tqdm
import torch
from matplotlib import pyplot as plt
import numpy as np
import GLOBALS
import crypten
from examples.meters import AverageMeter
from examples.multiprocess_launcher import MultiProcessLauncher

coeffs = [0.2734375, 0.5, 0.17578125, 0., -(2 ** (-5))]


def evalulate_poly_with_coeffs(x, coeffs):
    # print(x[98])
    x_enc = crypten.cryptensor(x, ptype=crypten.mpc.arithmetic)
    # print(x_enc.share[98])
    ans = x_enc.eval_poly(coeffs)
    return ans.get_plain_text()


def normal_evalulate_poly_with_coeffs(x, coeffs):
    # assert len(coeffs)==5, "Only for degree=4, for now"
    x_enc = crypten.cryptensor(x, ptype=crypten.mpc.arithmetic)
    x2 = x_enc * x_enc
    x4 = x2 * x2
    ans = coeffs[0] + coeffs[1] * x_enc + coeffs[2] * x2 + coeffs[4] * x4
    return ans.get_plain_text()


def get_range(start, end, step):
    return [start + i * step for i in range(int(ceil(end - start) / step))]


def experiment():
    # initialize crypten
    poly_location = "examples/mpc_resnet/model/d5_r5.0_rr5.0_g0.01_q1_cp8_coeffs.p"
    crypten.init()
    with open(poly_location, 'rb') as input_file:
        GLOBALS.COEFFS = pickle.load(input_file)
    while GLOBALS.COEFFS[-1] == 0.0:
        GLOBALS.COEFFS = GLOBALS.COEFFS[:-1]
    INCREMENT = 2 ** (-8)
    LOW = -2 
    HIGH = 2 + INCREMENT
    x_input = get_range(LOW, HIGH, INCREMENT)
    crypten_normal_poly = normal_evalulate_poly_with_coeffs(x_input, coeffs)
    plain_poly = torch.tensor([sum([x ** i * coeffs[i] for i in range(len(coeffs))]) for x in x_input])
    rank = crypten.comm.get().get_rank()
    error_average = AverageMeter()
    error_max = 0

    if rank == 0:
        seed = 3115+6865
        pbar = tqdm()
    else:
        seed = 3024+6865
    while True:
        torch.manual_seed(seed)
        np.random.seed(seed)
        random.seed(seed)
        crypten.manual_seed(seed, seed, seed)
        crypten_fast_poly = evalulate_poly_with_coeffs(x_input, coeffs)
        error = (plain_poly - crypten_fast_poly).abs().max()
        if rank == 0:
            OPACITY = 1
            pbar.update(1)
            error_average.add(error, 1)
            error_max = max(error_max, error)
            pbar.set_description("{:1.6f} ({:1.6f}) < {:1.6f}".format(error, error_average.value(), error_max))
            if error > 0.1:
                # print((plain_poly - crypten_fast_poly).abs().argmax())
                plt.plot(x_input, crypten_fast_poly, "--", label='CrypTen Fast Poly', alpha=OPACITY)
                plt.plot(x_input, crypten_normal_poly, "--", label='CrypTen Normal Poly', alpha=OPACITY)
                plt.plot(x_input, plain_poly, "--", label='Plain', alpha=OPACITY)
                plt.legend()
                plt.show()
                break
        seed += 1 



def main():
    launcher = MultiProcessLauncher(2, experiment)
    launcher.start()
    launcher.join()
    launcher.terminate()


if __name__ == "__main__":
    main()
