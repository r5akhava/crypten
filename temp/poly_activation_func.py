import numpy as np
import torch
import torch.nn as nn

''' Modified from: 
https://github.com/kvgarimella/sisyphus-ppml/blob/main/experiments/poly_regression/generate_poly_regression_coeffs.py'''
def generate_coeffs(activation_func, degree, range, granularity=1e-3):
    steps = int(2 * range / granularity)
    xs = np.linspace(-range, range, steps)
    ys = activation_func(xs)
    coeffs = np.polyfit(xs, ys, deg=degree)[::-1]
    return coeffs

''' Modified from: 
https://github.com/kvgarimella/sisyphus-ppml/blob/main/experiments/poly_regression/polynomial_regression_approx.py'''
class ReLUPolyApprox(nn.Module):
    def relu(self, x):
        return np.maximum(0, x)

    def __init__(self, degree=2, range=10, clip=-1):
        super(ReLUPolyApprox, self).__init__()
        self.coeffs = torch.tensor(np.array([generate_coeffs(self.relu, degree, range)])).squeeze()
        self.degree = degree
        self.clip = clip
        self.range = range

    def forward(self, x):
        out = 0
        if self.clip > 0:
            x = torch.clamp(x, -self.clip, self.clip)
        for i in range(self.degree + 1):
            out = out + self.coeffs[i] * x**i
        return out

class SigmoidPolyApprox(nn.Module):
    def sigmoid(self, x):
        return 1 / (1 + np.exp(-x))

    def __init__(self, degree=2, range=10, clip=-1):
        super(SigmoidPolyApprox, self).__init__()
        self.coeffs = torch.tensor(np.array([generate_coeffs(self.sigmoid, degree, range)])).squeeze()
        self.degree  = degree
        self.clip = clip
        self.range = range

    def forward(self, x):
        out = 0
        if self.clip:
            x = torch.clamp(x, -self.clip, self.clip)
        for i in range(self.degree + 1):
            out = out + self.coeffs[i] * x**i
        return out

if __name__ == "__main__":
    r = ReLUPolyApprox()
    s = SigmoidPolyApprox()
