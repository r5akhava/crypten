#!/usr/bin/env python3
import torch.nn as nn
import torch
# from dnn_sec_inference.utils.poly_activation_funcs import SigmoidPolyApprox, ReLUPolyApprox
from .poly_activation_funcs import SigmoidPolyApprox, ReLUPolyApprox

class MLP(nn.Module):
    def __init__(self, args):
        super(MLP, self).__init__()
        self.fc1 = nn.Linear(28 * 28, 512)
        # linear layer (n_hidden -> hidden_2)
        self.fc2 = nn.Linear(512, 512)
        # linear layer (n_hidden -> 10)
        self.fc3 = nn.Linear(512, 10)

        self.args = args
        if args.act_type == 'relu':
            self.act_func = torch.nn.ReLU()
        elif args.act_type == 'sig':
            self.act_func = torch.nn.Sigmoid()
        elif args.act_type == 'relu_poly':
            self.act_func = ReLUPolyApprox(args.file_prefix, args.degree, args.range, clip=args.clip)
        elif args.act_type == 'sig_poly':
            self.act_func = SigmoidPolyApprox(args.file_prefix, args.degree, args.range, clip=args.clip)
        else:
            raise NotImplementedError

        self.device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        self.loss = torch.nn.CrossEntropyLoss()

    def set_clip(self, new_clip):
        for m in self.modules():
            if isinstance(m, (ReLUPolyApprox, SigmoidPolyApprox)):
                m.clip = new_clip

    def swap_act(self, epoch, activations):
        def refresh_hooks():
            activations.remove_hooks()
            activations_to_regularize = ['act_func']
            for activation in activations_to_regularize:
                activations.register_reg_hooks(activation)

            activations_to_monitor = [('act_func', (-self.args.range, self.args.range))]
            for activation, input_range in activations_to_monitor:
                activations.register_monitor_hook(activation, input_range)

        if self.args.epoch_to_swap > 0:
            if epoch == self.args.epoch_to_swap:
                for m in self.modules():
                    if hasattr(m, 'act_func'):
                        if isinstance(m.act_func, torch.nn.ReLU):
                            m.act_func = ReLUPolyApprox(self.args.degree, self.args.range, clip=self.args.clip)
                            refresh_hooks()
                        elif isinstance(m.act_func, torch.nn.Sigmoid):
                            m.act_func = SigmoidPolyApprox(self.args.degree, self.args.range, clip=self.args.clip)
                            refresh_hooks()
                        else:
                            raise NotImplementedError("Need to start with a non-poly activation when hot swapping")

    def forward(self, x):
        # flatten image input
        x = x.view(-1, 28 * 28)
        # add hidden layer, with relu activation function
        x = self.act_func(self.fc1(x))
        x = self.act_func(self.fc2(x))
        x = self.fc3(x)
        return x
