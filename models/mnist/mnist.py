from torch.utils import data
from torchvision import transforms, datasets

from .imageset import ImageSet


class MNIST(ImageSet):

    name = 'mnist'
    num_classes = 10
    data_shape = [1, 28, 28]

    def __init__(self, root="./data", **kwargs):
        super().__init__(**kwargs)
        self.train_set = datasets.MNIST(root=root,
                                        transform=self.get_transform(mode="train"),
                                        train=True, download=True)
        self.valid_set = datasets.MNIST(root=root,
                                        transform=self.get_transform(mode="valid"),
                                        train=False, download=True)

    def get_dataloader(self, mode: str, batch_size: int = 16, shuffle: bool = True, num_workers: int = 8, **kwargs):
        if mode == "train":
            return data.DataLoader(self.train_set, batch_size=batch_size, shuffle=shuffle, num_workers=num_workers,
                                   **kwargs)
        elif mode == "valid":
            return data.DataLoader(self.valid_set, batch_size=batch_size, shuffle=shuffle, num_workers=num_workers,
                                   **kwargs)
        else:
            raise ValueError(f"Mode {mode} does not exist for MNIST")

    def get_transform(self, mode: str, **kwargs):
        if mode != 'train':
            return transforms.Compose([transforms.ToTensor(),
                                       transforms.Normalize((0.1307,), (0.3081,))])
        transform_list = [
            transforms.ToTensor(),
            transforms.Normalize((0.1307,), (0.3081,)),
            # transforms.Resize(224)
        ]
        #transform_list.append(transforms.ToTensor())
        return transforms.Compose(transform_list)
