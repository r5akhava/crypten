import numpy as np
import torch
import torch.nn as nn
import pickle
import os


def generate_coeffs(activation_func, file_prefix, degree, range, granularity=1e-3):
    filename = f"{file_prefix}_{granularity}_coeffs.p"
    if os.path.exists(filename):
    #Load from file
        with open(filename, 'rb') as input_file:
            coeffs = pickle.load(input_file)
    else:
        ''' Modified from: 
        https://github.com/kvgarimella/sisyphus-ppml/blob/main/experiments/poly_regression/generate_poly_regression_coeffs.py'''
        steps = int(2 * range / granularity)
        xs = np.linspace(-range, range, steps)
        ys = activation_func(xs)
        coeffs = np.polyfit(xs, ys, deg=degree)[::-1]
        with open(filename, 'wb') as out_file:
            pickle.dump(coeffs, out_file)
    return coeffs


def eval_poly(x, coeffs):
    out = 0
    for i, coef in enumerate(coeffs):
        out = out + coef * x ** i
    return out


''' Modified from: 
https://github.com/kvgarimella/sisyphus-ppml/blob/main/experiments/poly_regression/polynomial_regression_approx.py'''
class ReLUPolyApprox(nn.Module):
    def real_relu(self, x):
        return np.maximum(0, x)

    def __init__(self, file_prefix, degree=2, range=10, clip=-1):
        super(ReLUPolyApprox, self).__init__()
        self.coeffs = torch.tensor(np.array([generate_coeffs(self.real_relu, file_prefix, degree, range)])).squeeze()
        self.degree = degree
        self.clip = clip
        self.range = range

    def forward(self, x):
        out = 0
        if self.clip > 0:
            x = torch.clamp(x, -self.clip, self.clip)
        for i in range(self.degree + 1):
            out = out + self.coeffs[i] * x**i
        return out


class SigmoidPolyApprox(nn.Module):
    def real_sigmoid(self, x):
        return 1 / (1 + np.exp(-x))

    def __init__(self, file_prefix, degree=2, range=10, clip=-1):
        super(SigmoidPolyApprox, self).__init__()
        self.coeffs = torch.tensor(np.array([generate_coeffs(self.real_sigmoid, file_prefix, degree, range)])).squeeze()
        self.degree  = degree
        self.clip = clip
        self.range = range

    def forward(self, x):
        out = 0
        if self.clip:
            x = torch.clamp(x, -self.clip, self.clip)
        for i in range(self.degree + 1):
            out = out + self.coeffs[i] * x**i
        return out

# if __name__ == "__main__":
#     r = ReLUPolyApprox()
#     s = SigmoidPolyApprox()
