import numpy as np
import torch
import torch.nn as nn
import pickle
import os
from gekko import GEKKO

'''Fits a polynomial to approximate the given activation function
Saves the coefficients to a file '''


def generate_coeffs(activation_func, file_prefix, degree, rng, granularity=1e-2, quantized_coef=0,
                    crypto_precision=8):
    filename = f"{file_prefix}_g{granularity}_q{quantized_coef}_cp{crypto_precision}_coeffs.p"
    if os.path.exists(filename):
        # Load from file
        with open(filename, 'rb') as input_file:
            coeffs = pickle.load(input_file)
    else:
        ''' Modified from: 
        https://github.com/kvgarimella/sisyphus-ppml/blob/main/experiments/poly_regression/generate_poly_regression_coeffs.py'''
        steps = int(2 * rng / granularity)
        xs = np.linspace(-rng, rng, steps)
        ys = activation_func(xs)
        if quantized_coef:
            coeffs = quantized_poly_fit(xs, ys, degree,
                                        crypto_precision=crypto_precision)
        else:
            coeffs = np.polyfit(xs, ys, deg=degree)[::-1]
        with open(filename, 'wb') as out_file:
            pickle.dump(coeffs, out_file)
    return coeffs


'''Shifts the poly fit by the precision then fits with integer programing
This ensure we find the best coeffs under precision constraints'''


def quantized_poly_fit(xs, ys, degree, crypto_precision=8):
    X = []
    for i in range(degree + 1):
        X.append(np.power(xs, i))
    X = np.vstack(X)
    X = X.T
    # X = np.trunc(np.multiply(X, 2 ** crypto_precision)).astype(int)
    ys = np.trunc(np.multiply(ys, 2 ** crypto_precision)).astype(int)

    m = GEKKO()  # Initialize gekko
    m.options.SOLVER = 1  # APOPT is an MINLP solver

    # optional solver settings with APOPT
    m.solver_options = [
        #                       'minlp_maximum_iterations 500', \
        #                     # minlp iterations with integer solution
        # 'minlp_max_iter_with_int_sol 500', \
        #                     # treat minlp as nlp
        #                     'minlp_as_nlp 0', \
        #                     # nlp sub-problem max iterations
        #                     'nlp_maximum_iterations 50', \
        #                     # 1 = depth first, 2 = breadth first
        #                     'minlp_branch_method 1', \
        #                     # maximum deviation from whole number
        #                     'minlp_integer_tol 0.05', \
        #                     # covergence tolerance
        #                     'minlp_gap_tol 0.01'
    ]

    # Initialize variables
    alpha = [m.Var(integer=True, lb=-2 ** (crypto_precision - 1), ub=2 ** (crypto_precision - 1)) for _ in
             range(len(X[0]))]
    result = []
    for i in range(len(X)):
        sum_val = 0
        for j in range(len(X[0])):
            sum_val += X[i][j] * alpha[j]
        result.append(m.Intermediate((sum_val - ys[i]) ** 2))
    m.Obj(sum(result))  # Objective
    m.solve(disp=False)  # Solve
    alpha = np.array([x.value[0] for x in alpha])
    return alpha / (2 ** crypto_precision)


def eval_poly(x, coeffs):
    out = 0
    for i, coef in enumerate(coeffs):
        out = out + coef * x ** i
    return out


''' Modified from: 
https://github.com/kvgarimella/sisyphus-ppml/blob/main/experiments/poly_regression/polynomial_regression_approx.py'''


class ReLUPolyApprox(nn.Module):
    def real_relu(self, x):
        return np.maximum(0, x)

    def __init__(self, degree=2, rng=10, clip=-1, **kwargs):
        super(ReLUPolyApprox, self).__init__()

        quantized_coef = kwargs.get("quantized_coef")
        if quantized_coef is not None:
            self.kwargs = kwargs
            file_prefix = kwargs.get("file_prefix")
            granularity = kwargs.get("granularity")
            crypto_precision = kwargs.get("crypto_precision")
            self.coeffs = torch.tensor(np.array([generate_coeffs(self.real_relu, file_prefix, degree, rng,
                                                                 granularity=granularity,
                                                                 crypto_precision=crypto_precision)])).squeeze()
        else:
            # file_prefix = "Coeff_Evaluation/"
            # self.coeffs = torch.tensor(np.array([generate_coeffs(self.real_relu, file_prefix, degree, rng)])).squeeze()
            import GLOBALS
            self.coeffs = torch.tensor(GLOBALS.COEFFS)
            self.kwargs = {}
        self.degree = len(self.coeffs) - 1
        self.clip = clip
        self.rng = rng

    def forward(self, x):
        out = 0
        if self.clip > 0:
            x = torch.clamp(x, -self.clip, self.clip)
        for i in range(self.degree + 1):
            out = out + self.coeffs[i] * x ** i
        return out


class SigmoidPolyApprox(nn.Module):
    def real_sigmoid(self, x):
        return 1 / (1 + np.exp(-x))

    def __init__(self, file_prefix, degree=2, rng=10, clip=-1):
        super(SigmoidPolyApprox, self).__init__()
        self.coeffs = torch.tensor(np.array([generate_coeffs(self.real_sigmoid, file_prefix, degree, range)])).squeeze()
        self.degree = degree
        self.clip = clip
        self.rng = rng

    def forward(self, x):
        out = 0
        if self.clip:
            x = torch.clamp(x, -self.clip, self.clip)
        for i in range(self.degree + 1):
            out = out + self.coeffs[i] * x ** i
        return out


def real_relu(x):
    return np.maximum(0, x)


if __name__ == "__main__":
    print("HERE")
    crypto_precision = 16
    for degree in [2, 3, 4, 6, 8, 10]:
        for rng in [2.0, 5.0]:
            prefix = '{}/a{}_d{}_r{}'.format('Coeff_Evaluation', 'relu_poly',
                                             degree, rng)
            try:
                print(
                    f'd{degree}_r{int(rng)}:{generate_coeffs(real_relu, prefix, degree, rng, quantized_coef=0, crypto_precision=crypto_precision)}')
                print(
                    f'd{degree}_r{int(rng)}:{generate_coeffs(real_relu, prefix, degree, rng, quantized_coef=1, crypto_precision=crypto_precision)}')
            except Exception as e:
                print("HAHA:", e)
                pass
