import torch
import torch.nn as nn
from .poly_activation_funcs import ReLUPolyApprox, SigmoidPolyApprox


def get_act_func(act_type: str, degree: float, rnge: float, clip: float, **kwargs):
    if act_type == 'relu':
        return torch.nn.ReLU()
    elif act_type == 'sig':
        return torch.nn.Sigmoid()
    elif act_type == 'relu_poly':
        return ReLUPolyApprox(degree=degree, rng=rnge, clip=clip, **kwargs)
    elif act_type == 'sig_poly':
        return SigmoidPolyApprox(degree, rnge, clip=clip)
    else:
        raise NotImplementedError


class MiniONN(nn.Module):
    def __init__(self, act_type: str = "relu", degree: float = 5, rnge: float = 5., clip: float = 5., **kwargs):
        super().__init__()

        self.conv1 = nn.Conv2d(in_channels=3, out_channels=64, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1))
        self.conv2 = nn.Conv2d(in_channels=64, out_channels=64, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1))
        self.conv3 = nn.Conv2d(in_channels=64, out_channels=64, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1))
        self.conv4 = nn.Conv2d(in_channels=64, out_channels=64, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1))
        self.conv5 = nn.Conv2d(in_channels=64, out_channels=64, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1))
        self.conv6 = nn.Conv2d(in_channels=64, out_channels=64, kernel_size=(1, 1), stride=(1, 1))
        self.conv7 = nn.Conv2d(in_channels=64, out_channels=16, kernel_size=(1, 1), stride=(1, 1))

        self.fully_connected_layer = nn.Linear(in_features=1024, out_features=10)

        self.mean_pooling = nn.AvgPool2d(kernel_size=(2, 2))

        self.act_func = get_act_func(act_type, degree, rnge, clip, **kwargs)

        self.device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        self.loss = torch.nn.CrossEntropyLoss()

    def set_clip(self, new_clip):
        if isinstance(self.act_func, (ReLUPolyApprox, SigmoidPolyApprox)):
            self.act_func.clip = new_clip

    def forward(self, x):

        x = self.act_func(self.conv1(x))
        x = self.mean_pooling(self.act_func(self.conv2(x)))
        x = self.act_func(self.conv3(x))
        x = self.mean_pooling(self.act_func(self.conv4(x)))
        x = self.act_func(self.conv5(x))
        x = self.act_func(self.conv6(x))
        x = self.act_func(self.conv7(x))

        x = torch.flatten(x, 1)  # flatten all dimensions except batch
        x = self.fully_connected_layer(x)

        return x
