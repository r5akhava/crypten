from typing import Type, Union, List

import torch
import torch.nn as nn
import torch.nn.functional as F

from .poly_activation_funcs import ReLUPolyApprox, SigmoidPolyApprox


def get_act_func(act_type: str, degree: float, rnge: float, clip: float, **kwargs):
    if act_type == 'relu':
        return torch.nn.ReLU()
    elif act_type == 'sig':
        return torch.nn.Sigmoid()
    elif act_type == 'relu_poly':
        return ReLUPolyApprox(degree=degree, rng=rnge, clip=clip, **kwargs)
    elif act_type == 'sig_poly':
        return SigmoidPolyApprox(degree, rnge, clip=clip)
    else:
        raise NotImplementedError


class BasicBlock(nn.Module):
    expansion = 1

    def __init__(self, in_planes, planes, act_type: str = "relu",
                 degree: float = 10., rnge: float = 10., clip: float = 7.5, stride=1, **kwargs):
        super(BasicBlock, self).__init__()
        self.conv1 = nn.Conv2d(
            in_planes, planes, kernel_size=3, stride=stride, padding=1, bias=False)
        self.bn1 = nn.BatchNorm2d(planes)
        self.conv2 = nn.Conv2d(planes, planes, kernel_size=3,
                               stride=1, padding=1, bias=False)
        self.bn2 = nn.BatchNorm2d(planes)
        self.act_type = act_type
        self.act_func = get_act_func(act_type, degree, rnge, clip, **kwargs)
        self.shortcut = nn.Sequential()
        if stride != 1 or in_planes != self.expansion*planes:
            self.shortcut = nn.Sequential(
                nn.Conv2d(in_planes, self.expansion*planes,
                          kernel_size=1, stride=stride, bias=False),
                nn.BatchNorm2d(self.expansion*planes)
            )

    def forward(self, x):
        out = self.act_func(self.bn1(self.conv1(x)))
        out = self.bn2(self.conv2(out))
        out += self.shortcut(x)
        out = self.act_func(out)
        return out


class Bottleneck(nn.Module):
    expansion = 4

    def __init__(self, in_planes, planes, act_type: str = "relu",
                 degree: float = 10., rnge: float = 10., clip: float = 7.5, stride=1, **kwargs):
        super(Bottleneck, self).__init__()
        self.conv1 = nn.Conv2d(in_planes, planes, kernel_size=1, bias=False)
        self.bn1 = nn.BatchNorm2d(planes)
        self.conv2 = nn.Conv2d(planes, planes, kernel_size=3,
                               stride=stride, padding=1, bias=False)
        self.bn2 = nn.BatchNorm2d(planes)
        self.conv3 = nn.Conv2d(planes, self.expansion *
                               planes, kernel_size=1, bias=False)
        self.bn3 = nn.BatchNorm2d(self.expansion*planes)
        self.act_type = act_type
        self.act_func = get_act_func(act_type, degree, rnge, clip, **kwargs)

        self.shortcut = nn.Sequential()
        if stride != 1 or in_planes != self.expansion*planes:
            self.shortcut = nn.Sequential(
                nn.Conv2d(in_planes, self.expansion*planes,
                          kernel_size=1, stride=stride, bias=False),
                nn.BatchNorm2d(self.expansion*planes)
            )

    def forward(self, x):
        out = self.act_func(self.bn1(self.conv1(x)))
        out = self.act_func(self.bn2(self.conv2(out)))
        out = self.bn3(self.conv3(out))
        out += self.shortcut(x)
        out = self.act_func(out)
        return out


class ResNet(nn.Module):
    def __init__(self, block: Type[Union[BasicBlock, Bottleneck]], num_blocks: List[int], act_type: str = "relu",
                 degree: int = 5, rnge: float = 10., clip: float = 7.5,  num_classes: int = 10,
                 zero_init_residual: bool = False, **kwargs):
        super(ResNet, self).__init__()
        self.in_planes = 64
        self.layers = num_blocks

        self.act_type = act_type

        self.act_func = get_act_func(act_type=act_type, degree=degree, rnge=rnge, clip=clip, **kwargs)

        self.conv1 = nn.Conv2d(3, 64, kernel_size=3,
                               stride=1, padding=1, bias=False)
        self.bn1 = nn.BatchNorm2d(64)
        self.layer1 = self._make_layer(block, 64, num_blocks[0], act_type, degree, rnge, clip, stride=1, **kwargs)
        self.layer2 = self._make_layer(block, 128, num_blocks[1], act_type, degree, rnge, clip, stride=2, **kwargs)
        self.layer3 = self._make_layer(block, 256, num_blocks[2], act_type, degree, rnge, clip, stride=2, **kwargs)
        self.layer4 = self._make_layer(block, 512, num_blocks[3], act_type, degree, rnge, clip, stride=2, **kwargs)
        self.linear = nn.Linear(512*block.expansion, num_classes)

        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                nn.init.kaiming_normal_(m.weight, mode="fan_out", nonlinearity="relu")
                # nn.init.constant_(m.weight, 0.00001)
            elif isinstance(m, (nn.BatchNorm2d, nn.GroupNorm)):
                nn.init.constant_(m.weight, 1)
                nn.init.constant_(m.bias, 0)

        # Zero-initialize the last BN in each residual branch,
        # so that the residual branch starts with zeros, and each residual block behaves like an identity.
        # This improves the model by 0.2~0.3% according to https://arxiv.org/abs/1706.02677
        if zero_init_residual:
            for m in self.modules():
                if isinstance(m, Bottleneck):
                    nn.init.constant_(m.bn3.weight, 0)  # type: ignore[arg-type]
                elif isinstance(m, BasicBlock):
                    nn.init.constant_(m.bn2.weight, 0)  # type: ignore[arg-type]

    def _make_layer(self, block, planes, num_blocks, act_type, degree, rnge, clip, stride, **kwargs):
        strides = [stride] + [1]*(num_blocks-1)
        layers = []
        for stride in strides:
            layers.append(block(self.in_planes, planes, act_type, degree, rnge, clip, stride, **kwargs))
            self.in_planes = planes * block.expansion
        return nn.Sequential(*layers)

    def set_clip(self, new_clip):
        for m in self.modules():
            if isinstance(m, (ReLUPolyApprox, SigmoidPolyApprox)):
                m.clip = new_clip

    def forward(self, x):
        out = self.act_func(self.bn1(self.conv1(x)))
        out = self.layer1(out)
        out = self.layer2(out)
        out = self.layer3(out)
        out = self.layer4(out)
        out = F.avg_pool2d(out, 4)
        out = out.view(out.size(0), -1)
        out = self.linear(out)
        return out


def ResNet18(act_type: str = "relu", degree: int = 5, rnge: float = 10., clip: float = 7.5, **kwargs):
    return ResNet(BasicBlock, [2, 2, 2, 2], act_type=act_type, degree=degree, rnge=rnge, clip=clip, **kwargs)


def ResNet32(act_type: str = "relu", degree: int = 10., rnge: float = 10., clip: float = 7.5, **kwargs):
    return ResNet(BasicBlock, [5, 5, 5, 0], act_type=act_type, degree=degree, rnge=rnge, clip=clip, **kwargs)


def ResNet34(act_type: str = "relu", degree: int = 5, rnge: float = 10., clip: float = 7.5, **kwargs):
    return ResNet(BasicBlock, [3, 4, 6, 3], act_type=act_type, degree=degree, rnge=rnge, clip=clip, **kwargs)


def ResNet50(act_type: str = "relu", degree: int = 5, rnge: float = 10., clip: float = 7.5, **kwargs):
    return ResNet(Bottleneck, [3, 4, 6, 3], act_type=act_type, degree=degree, rnge=rnge, clip=clip, **kwargs)


def ResNet101(act_type: str = "relu", degree: int = 5, rnge: float = 10., clip: float = 7.5, **kwargs):
    return ResNet(Bottleneck, [3, 4, 23, 3], act_type=act_type, degree=degree, rnge=rnge, clip=clip, **kwargs)


def ResNet110(act_type: str = "relu", degree: int = 5, rnge: float = 10., clip: float = 7.5, **kwargs):
    return ResNet(BasicBlock, [18, 18, 18, 0], act_type=act_type, degree=degree, rnge=rnge, clip=clip, **kwargs)


def ResNet152(act_type: str = "relu", degree: int = 5, rnge: float = 10., clip: float = 7.5, **kwargs):
    return ResNet(Bottleneck, [3, 8, 36, 3], act_type=act_type, degree=degree, rnge=rnge, clip=clip, **kwargs)


def test():
    net = ResNet18()
    y = net(torch.randn(1, 3, 32, 32))
    print(y.size())