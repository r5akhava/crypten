import json
import matplotlib.pyplot as plt

delays = [0.0, 0.05, 0.1, 0.2, 0.5, 1.0]
relus = ['florian_relu', 'poly_relu', 'relu']
keys = ["rounds", "bytes", "comm time", "total time"]
folder = "/home/diaa/results"
# folder = "examples/mpc_resnet"
clients = 2
aggregate_results = {k: {} for k in keys}
for relu in relus:
    for key in keys:
        aggregate_results[key][relu] = []
    for delay in delays:
        res = {k: 0 for k in keys}
        for rank in range(clients):
            filename = f"{folder}/timing_{delay}_{relu}_{rank}.json"
            with open(filename, 'r') as f:
                loaded = json.load(f)
            for key in res:
                res[key] += loaded[key] / clients
        for key in keys:
            aggregate_results[key][relu].append(res[key])

for key in keys:
    for relu in relus:
        plt.plot(delays, aggregate_results[key][relu], label=relu)
        plt.title(key)
        plt.xlabel("Artificial Delay (s)")
    plt.legend()
    plt.show()