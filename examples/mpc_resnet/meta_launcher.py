import os

delays = [0.0, 0.05, 0.1, 0.2, 0.5, 1]
relus = ['florian_relu', 'poly_relu', 'relu']

for relu in relus:
    for delay in delays:
        os.system(f"python3 launcher.py --multiprocess --relu-mode {relu} --delay {delay}")