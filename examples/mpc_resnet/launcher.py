#!/usr/bin/env python3

# Copyright (c) Facebook, Inc. and its affiliates.
#
# This source code is licensed under the MIT license found in the
# LICENSE file in the root directory of this source tree.

import argparse
import logging
import os

from examples.multiprocess_launcher import MultiProcessLauncher

parser = argparse.ArgumentParser(description="CrypTen Cifar Training")
parser.add_argument(
    "-b",
    "--batch-size",
    default=8,
    type=int,
    metavar="N",
    help="mini-batch size (default: 4)",
)

parser.add_argument(
    "-n",
    "--n-batches",
    default=5,
    type=int,
    metavar="N",
    help="Number of batches to test(default: 100)",
)

parser.add_argument(
    "-m",
    "--mode",
    default="sidebyside",
    type=str,
    choices=["sidebyside", "separate"],
    help="Evaluation mode"
)

parser.add_argument(
    "--model-location",
    default="examples/mpc_resnet/model/arelu_poly_d5_r5.0_resnet18_0.1_5_5.0_0.001_5.0_1_0.01_8_2.pth",
    type=str,
    metavar="PATH",
    help="path to latest checkpoint (default: none)",
)
parser.add_argument(
    "--poly-location",
    default="examples/mpc_resnet/model/d5_r5.0_rr5.0_g0.01_q1_cp8_coeffs.p",
    type=str,
    metavar="PATH",
    help="path to latest poly (default: none)",
)

parser.add_argument(
    "--seed", default=1337, type=int, help="seed for initializing training. "
)
parser.add_argument(
    "--delay", default=0.0, type=float, help="artificial delay"
)
parser.add_argument(
    "--relu-mode",
    type=str,
    choices=['relu', 'poly_relu', 'florian_relu'],
    default='florian_relu'
)

parser.add_argument(
    "--model_type",
    type=str,
    default="resnet18",
    choices=["resnet18", "resnet32", "resnet50", "resnet110", "minionn"]
)

"""
    Arguments for Multiprocess Launcher
"""
parser.add_argument(
    "--multiprocess",
    default=False,
    action="store_true",
    help="Run example in multiprocess mode",
)

parser.add_argument(
    "--world-size",
    type=int,
    default=2,
    help="The number of parties to launch. Each party acts as its own process",
)


def _run_experiment(args):
    # only import here to initialize crypten within the subprocesses

    from mpc_resnet import run_mpc_resnet
    # Only Rank 0 will display logs.
    level = logging.INFO
    # level = logging.DEBUG
    if "RANK" in os.environ and os.environ["RANK"] != "0":
        level = logging.CRITICAL
    logging.getLogger().setLevel(level)
    run_mpc_resnet(
        args.batch_size,
        args.model_location,
        args.poly_location,
        args.seed,
        args.mode,
        args.n_batches,
        args.model_type,
        relu_mode=args.relu_mode,
        delay=args.delay
    )


def main(run_experiment):
    args = parser.parse_args()
    if args.multiprocess:
        launcher = MultiProcessLauncher(args.world_size, run_experiment, args)
        launcher.start()
        launcher.join()
        launcher.terminate()
    else:
        run_experiment(args)


if __name__ == "__main__":
    main(_run_experiment)
