#!/usr/bin/env python3
import json
# Copyright (c) Facebook, Inc. and its affiliates.
#
# This source code is licensed under the MIT license found in the
# LICENSE file in the root directory of this source tree.

import logging
import os
import pickle
import random
import shutil
import time

import torch
import torch.nn as nn
import torch.optim
import torch.utils.data
import torch.utils.data.distributed

import GLOBALS
import crypten
import crypten.communicator as comm
from examples.meters import AverageMeter
from models.resnet.cifar import CIFAR10
from models.resnet.cifar_resnet import ResNet18, ResNet32, ResNet50, ResNet110
from models.minionn.minionn import MiniONN


def build_model(model_type: str = "resnet18", act_type: str = "relu_poly"):
    if model_type == "resnet18":
        model = ResNet18(act_type=act_type)
    elif model_type == "resnet32":
        model = ResNet32(act_type=act_type)
    elif model_type == "resnet50":
        model = ResNet50(act_type=act_type)
    elif model_type == "resnet110":
        model = ResNet110(act_type=act_type)
    elif model_type == "minionn":
        model = MiniONN(act_type=act_type)
    else:
        raise NotImplementedError

    return model


import numpy as np


def run_mpc_resnet(
        batch_size=32,
        model_location="",
        poly_location="",
        seed=None,
        mode="sidebyside",
        n_batches=100,
        model_type: str = "resnet18",
        relu_mode: str = "florian_relu",
        delay: float = 0.0
):
    if seed is not None:
        random.seed(seed)
        torch.manual_seed(seed)
        np.random.seed(seed)
    if os.path.exists(poly_location):
        with open(poly_location, 'rb') as input_file:
            GLOBALS.COEFFS = pickle.load(input_file)
            while GLOBALS.COEFFS[-1] == 0.0:
                GLOBALS.COEFFS = GLOBALS.COEFFS[:-1]
    else:
        GLOBALS.COEFFS = [0.11328125, 0.5, 0.41015625, 0., -0.05078125]
    GLOBALS.RELU_MODE = relu_mode
    GLOBALS.ARTIFICIAL_DELAY = delay
    logging.info(GLOBALS.COEFFS)
    logging.info(GLOBALS.RELU_MODE)
    logging.info(GLOBALS.ARTIFICIAL_DELAY)
    crypten.init()

    # model = ResNet18(act_type="relu_poly", degree=degree, range=rnge, **kwargs)
    # we overwrite the custom parameters in the poly_activation_funcs.py code
    model = build_model(model_type, act_type="relu_poly")

    # model = ResNet18(act_type="relu")
    criterion = nn.CrossEntropyLoss()
    if os.path.isfile(model_location):
        logging.info("=> loading checkpoint '{}'".format(model_location))
        checkpoint = torch.load(model_location, map_location=torch.device("cpu"))
        model.load_state_dict(checkpoint)
        logging.info(
            "=> loaded checkpoint '{}'".format(
                model_location
            )
        )
    else:
        raise IOError("=> no checkpoint found at '{}'".format(model_location))

    dataset = CIFAR10(root="../data/")
    val_loader = dataset.get_dataloader('valid', batch_size=batch_size, num_workers=2, pin_memory=False)
    input_size = get_input_size(val_loader)
    private_model = construct_private_model(input_size, model.state_dict(), model_type)
    if mode == "sidebyside":
        logging.info("===== Validating side-by-side ======")
        validate_side_by_side(val_loader, model, private_model, n_batches)
        # spam_until_explosion(val_loader, model, private_model, n_batches)
    else:
        logging.info("===== Evaluating plaintext ResNet network =====")
        validate(val_loader, model, criterion, 1)
        logging.info("===== Evaluating Private ResNet network =====")
        validate(val_loader, private_model, criterion, 1)


def validate_side_by_side(val_loader, plaintext_model, private_model, n_batches=2):
    """Validate the plaintext and private models side-by-side on each example"""
    # switch to evaluate mode
    import time
    from datetime import timedelta

    plaintext_model.eval()
    private_model.eval()

    accuracy_plain = AverageMeter()
    accuracy_enc = AverageMeter()
    average_error = AverageMeter()
    match = AverageMeter()
    inference_time = AverageMeter()
    communication_time = AverageMeter()
    total_time = AverageMeter()
    with torch.no_grad():
        for i, (input, target) in enumerate(val_loader):
            # compute output for plaintext
            output_plaintext = plaintext_model(input)
            # encrypt input and compute output for private
            # assumes that private model is encrypted with src=0
            # Outputs
            input_encr = encrypt_data_tensor_with_src(input)

            error = 1e9
            retries = 0
            while error > 1000 and retries < 1:
                crypten.comm.get().reset_communication_stats()
                start_time = time.monotonic()
                output_encr = private_model(input_encr)
                end_time = time.monotonic()
                comm_stats = crypten.comm.get().get_communication_stats()

                delta = timedelta(seconds=end_time - start_time)
                time_diff = delta.seconds + (delta.microseconds / 1e6)
                average_time_diff = time_diff / input.size(0)
                inference_time.add(average_time_diff, input.size(0))
                total_time.add(time_diff, 1)
                communication_time.add(comm_stats['time'])
                output_encr = output_encr.get_plain_text()
                # Error
                error = (output_encr - output_plaintext).abs().sum().sum() / (
                        output_plaintext.shape[0] * output_plaintext.shape[1])
                retries += 1
                # print(retries, error, average_time_diff)
            average_error.add(error, (output_plaintext.shape[0] * output_plaintext.shape[1]))
            # Predictions
            pred_plain = output_plaintext.argmax(dim=1)
            pred_enc = output_encr.argmax(dim=1)
            # Accuracies
            acc_plain = accuracy(output_plaintext, target)
            accuracy_plain.add(acc_plain[0], input.size(0))
            acc_enc = accuracy(output_encr, target)
            accuracy_enc.add(acc_enc[0], input.size(0))
            # Match
            mtch = sum(pred_plain == pred_enc) * 100 / input.size(0)
            match.add(mtch, input.size(0))
            # log all info
            logging.info("==============================")
            logging.info(f"Example {i}\t target = {target}")
            logging.info(f"Example {i}\t plainP = {pred_plain}")
            logging.info(f"Example {i}\t encryP = {pred_enc}")
            logging.info("   ========================   ")
            logging.info("Enc Acc    {:.3f} ({:.3f})".format(acc_enc[0].item(), accuracy_enc.value().item()))
            logging.info("Pla Acc    {:.3f} ({:.3f})".format(acc_plain[0].item(), accuracy_plain.value().item()))
            logging.info("Match      {:.3f} ({:.3f})".format(mtch.item(), match.value().item()))
            logging.info("Error      {:.3f} ({:.3f})".format(error.item(), average_error.value().item()))
            logging.info("Runtime    {:.3f} ({:.3f})".format(average_time_diff, inference_time.value()))
            logging.info("Rounds     {:.3f} ({:.3f})".format(comm_stats['rounds'], comm_stats['rounds']))
            logging.info("Bytes      {:.3f} ({:.3f})".format(comm_stats['bytes'], comm_stats['bytes']))
            logging.info("Comtime    {:.3f} ({:.3f})".format(comm_stats['time'], communication_time.value()))
            logging.info("Runtime(T) {:.3f} ({:.3f})".format(time_diff, total_time.value()))
            if i >= n_batches - 1:
                rank = crypten.comm.get().get_rank()
                file_name = f"timing_{GLOBALS.ARTIFICIAL_DELAY}_{GLOBALS.RELU_MODE}_{rank}.json"
                res = {"rounds": comm_stats['rounds'],
                       "bytes": comm_stats['bytes'],
                       "comm time": communication_time.value(),
                       "total time": inference_time.value()
                       }
                with open(file_name, 'w') as f:
                    json.dump(res, f)
                break


def spam_until_explosion(val_loader, plaintext_model, private_model, n_batches=100):
    """Validate the plaintext and private models side-by-side on each example"""
    # switch to evaluate mode
    plaintext_model.eval()
    private_model.eval()

    accuracy_plain = AverageMeter()
    accuracy_enc = AverageMeter()
    average_error = AverageMeter()
    match = AverageMeter()

    with torch.no_grad():
        for i, (input, target) in enumerate(val_loader):
            for _try in range(1):
                # compute output for plaintext
                output_plaintext = plaintext_model(input)
                # encrypt input and compute output for private
                # assumes that private model is encrypted with src=0
                # Outputs
                input_encr = encrypt_data_tensor_with_src(input)
                output_encr = private_model(input_encr).get_plain_text()
                # Error
                error = (output_encr - output_plaintext).abs().sum().sum() / (
                        output_plaintext.shape[0] * output_plaintext.shape[1])
                if error > 100:
                    print(">>>>>>>>>>>>><<<<<<<<<<<<<")
                    print(f"Example {i}, try {_try}")
                    print(">>>>>>>>>>>>><<<<<<<<<<<<<")
                    break
                average_error.add(error, (output_plaintext.shape[0] * output_plaintext.shape[1]))
                # Predictions
                pred_plain = output_plaintext.argmax(dim=1)
                pred_enc = output_encr.argmax(dim=1)
                # Accuracies
                acc_plain = accuracy(output_plaintext, target)
                accuracy_plain.add(acc_plain[0], input.size(0))
                acc_enc = accuracy(output_encr, target)
                accuracy_enc.add(acc_enc[0], input.size(0))
                # Match
                mtch = sum(pred_plain == pred_enc) * 100 / input.size(0)
                match.add(mtch, input.size(0))
                # log all info
                logging.info("==============================")
                logging.info(f"Example {i}\t target = {target}")
                logging.info(f"Example {i}\t plainP = {pred_plain}")
                logging.info(f"Example {i}\t encryP = {pred_enc}")
                logging.info("   ========================   ")
                logging.info("Enc Acc {:.3f} ({:.3f})".format(acc_enc[0].item(), accuracy_enc.value().item()))
                logging.info("Pla Acc {:.3f} ({:.3f})".format(acc_plain[0].item(), accuracy_plain.value().item()))
                logging.info("Match   {:.3f} ({:.3f})".format(mtch.item(), match.value().item()))
                logging.info("Error   {:.3f} ({:.3f})".format(error.item(), average_error.value().item()))
                logging.info("   ========================   ")
                logging.info("Plaintext:\n%s" % output_plaintext)
                logging.info("Encrypted:\n%s\n" % output_encr)


def get_input_size(val_loader):
    input, target = next(iter(val_loader))
    return input.size()


def construct_private_model(input_size, state_dict, model_type):
    """Encrypt and validate trained model for multi-party setting."""
    # get rank of current process
    rank = comm.get().get_rank()
    dummy_input = torch.empty(input_size)

    # party 0 always gets the actual model; remaining parties get dummy model
    # model_upd = ResNet18(act_type="relu")
    model_upd = build_model(model_type, act_type="relu")
    if rank == 0:
        model_upd.load_state_dict(state_dict)

    private_model = crypten.nn.from_pytorch(model_upd, dummy_input).encrypt(src=0)
    return private_model


def encrypt_data_tensor_with_src(input):
    """Encrypt data tensor for multi-party setting"""
    # get rank of current process
    rank = comm.get().get_rank()
    # get world size
    world_size = comm.get().get_world_size()

    if world_size > 1:
        # party 1 gets the actual tensor; remaining parties get dummy tensor
        src_id = 1
    else:
        # party 0 gets the actual tensor since world size is 1
        src_id = 0

    if rank == src_id:
        input_upd = input
    else:
        input_upd = torch.empty(input.size())
    private_input = crypten.cryptensor(input_upd, src=src_id)
    return private_input


def validate(val_loader, model, criterion, print_freq=10):
    batch_time = AverageMeter()
    losses = AverageMeter()
    top1 = AverageMeter()
    top5 = AverageMeter()

    # switch to evaluate mode
    model.eval()

    with torch.no_grad():
        end = time.time()
        for i, (input, target) in enumerate(val_loader):
            if isinstance(model, crypten.nn.Module) and not crypten.is_encrypted_tensor(
                    input
            ):
                input = encrypt_data_tensor_with_src(input)
            # compute output
            output = model(input)
            if crypten.is_encrypted_tensor(output):
                output = output.get_plain_text()
            loss = criterion(output, target)

            # measure accuracy and record loss
            prec1, prec5 = accuracy(output, target, topk=(1, 5))
            losses.add(loss.item(), input.size(0))
            top1.add(prec1[0], input.size(0))
            top5.add(prec5[0], input.size(0))

            # measure elapsed time
            current_batch_time = time.time() - end
            batch_time.add(current_batch_time)
            end = time.time()

            if (i + 1) % print_freq == 0:
                logging.info(
                    "\nTest: [{}/{}]\t"
                    "Time {:.3f} ({:.3f})\t"
                    "Loss {:.4f} ({:.4f})\t"
                    "Prec@1 {:.3f} ({:.3f})   \t"
                    "Prec@5 {:.3f} ({:.3f})".format(
                        i + 1,
                        len(val_loader),
                        current_batch_time,
                        batch_time.value(),
                        loss.item(),
                        losses.value(),
                        prec1[0],
                        top1.value(),
                        prec5[0],
                        top5.value(),
                    )
                )

        logging.info(
            " * Prec@1 {:.3f} Prec@5 {:.3f}".format(top1.value(), top5.value())
        )
    return top1.value()


def save_checkpoint(state, is_best, filename="checkpoint.pth.tar"):
    """Saves checkpoint of plaintext model"""
    # only save from rank 0 process to avoid race condition
    rank = comm.get().get_rank()
    if rank == 0:
        torch.save(state, filename)
        if is_best:
            shutil.copyfile(filename, "model_best.pth.tar")


def adjust_learning_rate(optimizer, epoch, lr=0.01):
    """Sets the learning rate to the initial LR decayed by 10 every 30 epochs"""
    new_lr = lr * (0.1 ** (epoch // 5))
    for param_group in optimizer.param_groups:
        param_group["lr"] = new_lr


def accuracy(output, target, topk=(1,)):
    """Computes the precision@k for the specified values of k"""
    with torch.no_grad():
        maxk = max(topk)
        batch_size = target.size(0)

        _, pred = output.topk(maxk, 1, True, True)
        pred = pred.t()
        correct = pred.eq(target.view(1, -1).expand_as(pred))

        res = []
        for k in topk:
            correct_k = correct[:k].flatten().float().sum(0, keepdim=True)
            res.append(correct_k.mul_(100.0 / batch_size))
        return res
